## Docker部署Zabbix+Grafana监控

>  环境 centos 7  ; Docker 17.12.0-ce ; docker-compose version 1.20.1  
>  2018-4-1 当前zabbix最新版3.4.7 ,grafana最新版5.0.4 


#### #下载部署代码
	cd /opt  
	git clone https://gitee.com/almi/zabbix-docker.git

>  主要是docker-compose编排文件，中文字体mi.ttc(小米兰亭)

#### #运行容器
``` #首次运行自动下载docker镜像  
cd zabbix-docker  
docker-compose -up -d

 #查看
docker-compose ps 
```
>  zabbix-snmp容器是zabbix snmp监控使用到  
>  zabbix-java容器用于zabbix监控tomcat jmx

![](https://images2018.cnblogs.com/blog/1239246/201804/1239246-20180403091643892-459851728.jpg)

#### #配置

 zabbix 登录 ，默认账户Admin 密码zabbix  
   设置中文 ：右上角Admin图标——Language——选择Chianses——Update

  grafana安装zabbix插件   

```
 #等待，若安装失败,多执行几次
  docker exec -it grafana grafana-cli plugins install alexanderzobnin-zabbix-app  

 # ✔ Installed alexanderzobnin-zabbix-app successfully 

 #重启grafana
 docker restart grafana
```

 - grafana登录 ip:3000 ，默认账户admin 密码admin  
 - 启用zabbix插件：Home -> Installed Apps (zabbix) -> Zabbix Plugin Config -> Enable  
 - 添加zabbix数据源：Add data source -> Type: Zabbix   
    ... -> URL：http:// IP地址/api_jsonrpc.php ，Access选择direct->zabbix账户密码


***
 #参考文档  

 Zabbix docker 官方说明文档  
https://www.zabbix.com/documentation/3.4/zh/manual/installation/containers

 #zabbix-docker官方git网址,很详细的docker-compose参考文档  
https://github.com/zabbix/zabbix-docker

 #grafane docker部署参考  
https://github.com/grafana/grafana-docker

***

 #这是目前最新版zabbix 3.4.7  ,可以自定义多个仪表盘
![](https://images2018.cnblogs.com/blog/1239246/201804/1239246-20180402230621508-373218691.jpg)


 #grafana可以设置很多图形  
![](https://images2018.cnblogs.com/blog/1239246/201804/1239246-20180402230651158-1817459390.jpg)


 #以下是我正在使用的版本zabbix3.2 (从zabbix 2.4升级到3.0，再升级到3.2)  
 #3.2主面板栏目可以最小化,  3.4最新版不能
![](https://images2018.cnblogs.com/blog/1239246/201804/1239246-20180402231409357-1392971919.jpg)

